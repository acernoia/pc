#include <systemc.h>
#include "PC.hpp"

using namespace std;

void PC::load()
{
    uscita->write(0);
    cout << "PROGRAM COUNTER INIZIALIZZATO A ZERO" << endl << endl;
    while(true)
    {
      wait();
        uscita->write(ingresso->read());
        cout << endl << "-->PROGRAM COUNTER = " << ingresso->read() << endl;
    }

}
