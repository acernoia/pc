#include <systemc.h>
#include "PC_inc.hpp"

using namespace std;

void PC_inc::incrementa()
{
   //Inizializzo il PC+1 a uno
   uscita->write(1);
   //cout << "PC+1 INIZIALIZZATO A UNO" << endl << endl;
   while(true)
   {
      wait();
      uscita->write(ingresso->read()+1);
   }

}
