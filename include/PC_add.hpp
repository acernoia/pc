#ifndef PC_ADD_HPP
#define PC_ADD_HPP

SC_MODULE(PC_add)
{
   sc_in<sc_uint<16> >  ingresso1;
   sc_in<sc_uint<16> >  ingresso2;
   sc_out<sc_uint<16> > uscita;

   SC_CTOR(PC_add)
   {
     SC_THREAD(somma);
     sensitive << ingresso1 << ingresso2;
   }
   private:
   void somma ();
};


#endif
