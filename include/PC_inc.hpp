#ifndef PC_INC_HPP
#define PC_INC_HPP

SC_MODULE(PC_inc)
{
   sc_in<sc_uint<16> >  ingresso;
   sc_out<sc_uint<16> > uscita;

   SC_CTOR(PC_inc)
   {
     SC_THREAD(incrementa);
     sensitive << ingresso;
   }
   private:
   void incrementa ();
};


#endif
