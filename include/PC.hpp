#ifndef PC_HPP
#define PC_HPP

SC_MODULE(PC)
{
   sc_in<bool>          CLK;
   sc_in<sc_uint<16> >  ingresso;
   sc_out<sc_uint<16> > uscita;

   SC_CTOR(PC)
   {
     SC_THREAD(load);
     sensitive << CLK.pos();
   }
   private:
   void load ();
};


#endif
