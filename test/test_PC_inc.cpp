#include <systemc.h>
#include <string>
#include "PC_inc.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > ingresso;
    sc_signal<sc_uint<16> > uscita;
    PC_inc inc;
    
    SC_CTOR(TestBench) : inc("inc")
    {
        SC_THREAD(stimulus_thread);
        inc.ingresso(this->ingresso);
        inc.uscita(this->uscita);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != (in_test[i]+1))
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << in_test[i]+1 << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void stimulus_thread() 
   {
        cout << "START STIMULUS" << endl << endl;
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            ingresso.write(in_test[i]);
            wait(1,SC_NS);
            dato_letto[i] = uscita.read(); 
            cout << "INGRESSO : " << ingresso.read() << endl;
            cout << "USCITA   : " << uscita.read() << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 4;
    // NOTA: short = 2 byte
    unsigned short in_test[TEST_SIZE];
    unsigned short dato_letto[TEST_SIZE];

    void init_values() 
    {
        in_test[0] = 0;
        in_test[1] = 1;
        in_test[2] = 49;
        in_test[3] = 4;
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START" << endl << endl;

  sc_start();

  return test.check();
}
