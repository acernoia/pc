#include <systemc.h>
#include <string>
#include "PC_add.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<sc_uint<16> > ingresso1;
    sc_signal<sc_uint<16> > ingresso2;
    sc_signal<sc_uint<16> > uscita;
    PC_add add;
    
    SC_CTOR(TestBench) : add("add")
    {
        SC_THREAD(stimulus_thread);
        add.ingresso1(this->ingresso1);
        add.ingresso2(this->ingresso2);
        add.uscita(this->uscita);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i] != (in1_test[i]+in2_test[i]))
            {
                cout << "TEST FALLITO: " << i << endl;
                cout << "RISULTATO TEORICO: " << in1_test[i]+in2_test[i] << endl;
                cout << "RISULTATO TEST :" << dato_letto[i] << endl;
                return 1;
             }
        }
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void stimulus_thread() 
   {
        cout << "START STIMULUS" << endl << endl;
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            ingresso1.write(in1_test[i]);
            ingresso2.write(in2_test[i]);
            wait(1,SC_NS);
            dato_letto[i] = uscita.read(); 
            cout << "INGRESSO 1 : " << ingresso1.read() << endl;
            cout << "INGRESSO 2 : " << ingresso2.read() << endl;
            cout << "USCITA    : " << uscita.read() << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 4;
    // NOTA: short = 2 byte
    unsigned short in1_test[TEST_SIZE];
    unsigned short in2_test[TEST_SIZE];
    unsigned short dato_letto[TEST_SIZE];

    void init_values() 
    {
        in1_test[0] = 0;
        in1_test[1] = 1;
        in1_test[2] = 0;
        in1_test[3] = 4;

        in2_test[0] = 1;
        in2_test[1] = 0;
        in2_test[2] = 0;
        in2_test[3] = 3;
    }


};

int sc_main(int argc, char* argv[])
{
  TestBench test("test");

  cout << "START" << endl << endl;

  sc_start();

  return test.check();
}
