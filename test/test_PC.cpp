#include <systemc.h>
#include <string>
#include "PC.hpp"

using namespace std;

SC_MODULE(TestBench)
{
    sc_signal<bool> clock;
    sc_signal<sc_uint<16> > ingresso;
    sc_signal<sc_uint<16> > uscita;
    PC pc;
    
    SC_CTOR(TestBench) : pc("pc")
    {
        SC_THREAD(clock_thread);
        SC_THREAD(stimulus_thread);
        pc.CLK(this->clock);
        pc.ingresso(this->ingresso);
        pc.uscita(this->uscita);
        init_values();
    }

    int check() 
    {

        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
            if (dato_letto[i]!=in_test[i])
            {
                cout << "TEST FALLITO      : " << i << endl;
                cout << "RISULTATO TEORICO : " << in_test[i] << endl;
                cout << "RISULTATO TEST    : " << dato_letto[i] << endl;
                return 1;
             }
        }

        cout << "CLOCK PERIOD : " << clock_period << " ns" << endl;
        cout << "CLOCK COUNT : " << clock_count/2 << endl;
        cout << "SIMULATION TIME : " << clock_period*clock_count/2 << " ns" << endl;
        cout << "TEST OK" << endl;                 
        return 0;
    }


  private:

   void clock_thread() 
   {

     bool value = false;
     while(true) 
     {
            clock.write(value);
            value = !value;
            clock_count++;
            wait(clock_period/2,SC_NS);
      }
   }

   void stimulus_thread() 
   { 
        cout << "START STIMULUS" << endl << endl;
        for (unsigned i=0;i<TEST_SIZE;i++) 
        {
           cout << "CLOCK COUNT : " << clock_count/2 << endl;
           ingresso.write(in_test[i]);
           wait(clock_period,SC_NS);
           dato_letto[i]=uscita.read();
           cout << "Dato scritto nel PC : " << ingresso.read() << endl;
           cout << "Dato letto dal PC   : " << uscita.read() << endl << endl;
        }
    }

    static const unsigned TEST_SIZE = 4;
    // NOTA: short = 2 byte
    unsigned short in_test[TEST_SIZE];
    unsigned short dato_letto[TEST_SIZE];
    unsigned clock_count;
    unsigned clock_period;
    void init_values() 
    {
       //Inizializzazione del contatore dei cicli di clock
       clock_count=0;
       //Periodo di clock in nanosecondi
       clock_period=20;

       in_test[0]=50;
       in_test[1]=2;
       in_test[2]=0;
       in_test[3]=47;

    }


};

int sc_main(int argc, char* argv[])
{
  int sim_time = 2000;

  TestBench test_PC("test_PC");

  sc_start(sim_time,SC_NS);

  return test_PC.check();
}
